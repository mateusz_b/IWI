/**
 * Created by Kamil on 22.10.2016.
 */
function getPersons(data, key) {
    return data.map(function (row) {
        if (row.type === 'person') {
            if (key === 'name') {
                return row[key]
            }
            else if (key === 'img_src') {
                return row[key];
            }
            else {
                return Math.round(row[key] * 1000000) / 1000000;
            }
        }

    });
}
function getTargetType(data) {
    for (var i = 0; i < data.length; i++) {
        if (data[i].type != 'person') {
            return data[i].type
        }
    }
}

function getTargetNode(data, key) {
    return data.map(function (row) {
        if (row.type != 'person') {
            if (key === 'name') {
                return row[key]
            }
            else {
                return Math.round(row[key] * 1000000) / 1000000;
            }
        }
    });
}

function getNodes(data, key) {
    return [Math.round(data.source[key] * 1000000) / 1000000, Math.round(data.target[key] * 1000000) / 1000000];
}
function setMode(data, peopleNodes) {
    data = data.filter(function(el){
        if (peopleNodes){
            return el.type =='person'
        }
        else {
            return el.type != 'person'
        }
    });
    if (data.length > 100) {
        return 'markers'
    }
    else {
        return'markers+text'
    }
}

function drawPlot(urlParams) {
    Plotly.d3.json('/data' + urlParams, function (err, rows) {
        var myPlot = document.getElementById('plot');
        var hoverImg = document.getElementById('hover-image');
        var hoverText = document.getElementById('name');
        var $hoverRelated = $('#related_nodes');

        // var imageUrl = "http://www.hit4hit.org/img/login/user-icon-6.png";
        var imageUrl = "http://localhost:8000/static/images/default.png";

        var data1 = {
            x: getPersons(rows[0], 'x'), y: getPersons(rows[0], 'y'), z: getPersons(rows[0], 'z'),
            mode: setMode(rows[0], true),
            marker: {
                color: 'rgb(179, 235, 66)',
                //color: 'rgb(220, 220, 220)',
                size: 6.5,
                line: {
                    color: 'rgba(148, 195, 53, 0.98)',
                    width: 0.5
                },
                opacity: 0
            },
            type: 'scatter3d',
            text: getPersons(rows[0], 'name'),
            imgSrc: getPersons(rows[0], 'img_src'),
            hoverinfo: 'name+text',
            name: 'Person'
        };
        var data2 = {
            x: getTargetNode(rows[0], 'x'), y: getTargetNode(rows[0], 'y'), z: getTargetNode(rows[0], 'z'),
            mode: setMode(rows[0], false),
            marker: {
                //color: 'rgb(153, 0, 0)',
                color: 'rgb(55,100,135)',
                size: 5,
                symbol: 'circle',
                line: {
                    color: 'rgba(255,100,135, 0.14)',
                    width: 1
                },
                opacity: 0
            },
            text: getTargetNode(rows[0], 'name'),
            type: 'scatter3d',
            hoverinfo: 'name+text',
            name: getTargetType(rows[0])
        };
        var edges = [];
        data = [data1, data2];

        for (var i = 0; i < rows[1].length; i++) {

            var edge = {
                type: 'scatter3d',
                mode: 'none',
                x: getNodes(rows[1][i], 'x'),
                y: getNodes(rows[1][i], 'y'),
                z: getNodes(rows[1][i], 'z'),
                opacity: 1,
                line: {
                    width: 1,
                    color: 'red',
                    reversescale: false
                },
                hoverinfo: 'name',
                name: 'Connection'
            };
            data.push(edge)
        }

        var layout = {
            margin: {
                l: 0,
                r: 0,
                b: 0,
                t: 0
            },
            scene: {
                xaxis: {
                    autorange: true,
                    showgrid: false,
                    zeroline: false,
                    showline: false,
                    autotick: true,
                    ticks: '',
                    showticklabels: false,
                    showspikes: false,
                    title: ""
                },
                yaxis: {
                    autorange: true,
                    showgrid: false,
                    zeroline: false,
                    showline: false,
                    autotick: true,
                    ticks: '',
                    showticklabels: false,
                    showspikes: false,
                    title: ""
                },
                zaxis: {
                    autorange: true,
                    showgrid: false,
                    zeroline: false,
                    showline: false,
                    autotick: true,
                    ticks: '',
                    showticklabels: false,
                    showspikes: false,
                    title: ""
                },
                aspectratio: {
                    x: 1.5,
                    y: 1.5,
                    z: 1.5
                }
            },
            showlegend: false,
            projection: {
                x: {
                    show: false
                },
                y: {
                    show: false
                },
                z: {
                    show: false
                },
            }

        };

        Plotly.newPlot('plot', data, layout);

        myPlot.on('plotly_click', function (eventData) {

            var point = eventData.points[0];

            if (point.data.marker) {
                var name = point.data.text[point.pointNumber];
                $.getJSON('/node_data?node=' + name, function (node_data) {
                    $hoverRelated.empty();
                    var first = true;
                    for (var key in node_data) {
                        if (node_data.hasOwnProperty(key)) {
                            var panelText = '<div class="panel panel-default">' +
                                '<div class="panel-heading">' +
                                '<h4 class="panel-title">' +
                                '<a data-toggle="collapse" data-parent="#related_nodes" href="#'+key+'">' +
                                key + '</a>' +
                                '</h4>' +
                                '</div>';
                            var targets = "";
                            for (var i=0; i<node_data[key].length; i++){
                                targets = targets + "<li>"+ node_data[key][i] + "</li>";
                            }
                            var isCollapsed = first ? 'in' : '';
                            targets = "<ul>"+targets+"</ul>";
                            $hoverRelated.append(
                                panelText + '<div id="' + key + '" class="panel-collapse collapse"'+ isCollapsed +'><div class="panel-body">'+ targets +'</div></div></div>'
                            );
                        }
                    }
                });
                hoverText.innerText = name;
                hoverImg.src = point.data.imgSrc ? point.data.imgSrc[point.pointNumber] : "";
                point = {
                    x: point.data.x[point.pointNumber],
                    y: point.data.y[point.pointNumber],
                    z: point.data.z[point.pointNumber],
                };
            } else if (point.data.line) {
                point.data.line.width = 20;
            }

            for (var i = 2; i < data.length; i++) {

                if (data[i].x[0] == point.x) {
                    data[i].mode = 'lines';
                    data[i].line.width = 3;
                    //data[i].line.color = 'rgb(55, 100, 136)';
                    //data[i].line.color = 'rgb(0, 51, 100)';
                    data[i].line.color = 'rgb(200, 0, 0)';
                }
                else if (data[i].x[1] == point.x && data[i].y[1] == point.y && data[i].z[1] == point.z) {
                    data[i].mode = 'lines';
                    data[i].line.width = 3;
                    data[i].line.color = 'rgb(200, 0, 0)';
                }
                else {
                    data[i].mode = 'none';
                    data[i].line.width = 1;
                    data[i].line.color = 'red';
                }
            }

            Plotly.animate('plot', {
                data: data,
                traces: [0],
                layout: layout
            }, {
                transition: {
                    duration: 500,
                    ease: 'cubic-in-out'
                }
            })
        });
    });
}

