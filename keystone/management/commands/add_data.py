from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from keystone.models import Edge, Node

from bs4 import BeautifulSoup

class Command(BaseCommand):
    help = 'Add data from xml to database'

    def add_arguments(self, parser):
        parser.add_argument('--filename')
        parser.add_argument('--delete')
    def handle(self, *args, **options):
        if options.get('delete'):
            Node.objects.all().delete()
            Edge.objects.all().delete()
        soup = BeautifulSoup(open(options['filename']), 'xml')
        nodes = soup.find_all('node')
        edges = soup.find_all('edge')
        print(len(nodes))
        print(len(edges))
        node_list = []
        for node in nodes:
            if node.get('type'):
                idx = node['id']
                type = node['type']
                href = node.get('href')
                name = node.get('name')
                node_list.append(Node(id=idx, type=type, href=href, name=name))
        Node.objects.bulk_create(node_list)
        edge_list = []
        for edge in edges:
            try:
                source = Node.objects.get(id=edge.get('source'))
                target = Node.objects.get(id=edge.get('target'))
            except Node.DoesNotExist:
                continue
            try:
                Edge.objects.create(source_node=source, target_node=target)
            except IntegrityError:
                print("Integrity error: %s-%s" % (source, target))

        self.stdout.write("Success!")



