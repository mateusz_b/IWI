# -*- coding: utf-8 -*-
from collections import defaultdict, Counter
from itertools import product, combinations, chain
from math import sin, cos, radians

import numpy

from .models import Edge, Node
import random, math
__author__ = 'mateuszb'


r_Max = 2
r_Min = 1
glob_direction = 'W'



def collect_pairs(list_of_targets):
    pair_counter = Counter()
    for l in list_of_targets:
        unique_tokens = sorted(set(l))
        combos = combinations(unique_tokens, 2)
        pair_counter += Counter(combos)
    return pair_counter


def create_connection_matrix(from_type, to_type):
    edges = Edge.objects.filter(source_node__type=from_type,
                                target_node__type=to_type) \
        .values_list('source_node__name', 'target_node__name')
    data_dict = defaultdict(list)
    for source_node, target_node in edges:
        data_dict[source_node].append(target_node)
    list_of_targets = []
    for k, v in data_dict.items():
        list_of_targets.append(v)
    total_list = chain(*list_of_targets)
    return Counter(total_list), collect_pairs(list_of_targets)


def calculate_points(n, radius_factor=1):
    golden_angle = numpy.pi * (3 - numpy.sqrt(5))
    theta = golden_angle * numpy.arange(n)
    z = numpy.linspace(1 - 1.0 / n, 1.0 / n - 1, n)
    radius = numpy.sqrt(1 - z * z) * radius_factor
    points = numpy.zeros((n, 3))
    points[:, 0] = radius * numpy.cos(theta)
    points[:, 1] = radius * numpy.sin(theta)
    points[:, 2] = z * radius_factor
    return points


def create_matrix(edges):
    source_nodes = list(set(edges.values_list('source_node', flat=True)))
    matrix_dict = dict()
    for node in source_nodes:
        matrix_dict[node] = defaultdict(lambda: 0)
        node_targets = edges.filter(source_node=node).values_list('target_node', flat=True)
        for target in node_targets:
            matrix_dict[node][target] += 1
    return matrix_dict

# losowanie kierunku, w ktorym przesuwany bedzie pkt
def direction_random():
    global glob_direction
    #print("global",glob_direction )

    choose = "WENS"
    rand = random.randint(0, 3)
    direction = choose[rand]

    # gwarantuje nam, że nie będziemy ponownie
    # poruszali się w tym samym kierunku
    while(glob_direction == direction):
        rand = random.randint(0, 3)
        direction = choose[rand]

    if rand == 0:
        glob_direction = 'W'
        return 'W'
    elif rand == 1:
        glob_direction = 'E'
        return 'E'
    elif rand == 2:
        glob_direction = 'N'
        return 'N'
    else:
        glob_direction = 'S'
        return 'S'

# ustawienie kata
def set_angle(direction, angle_delta):
    angle = radians(angle_delta)
    if direction == 'W':
        angle = angle_delta
    elif direction == 'E':
        angle = -angle_delta
    elif direction == 'N':
        angle = angle_delta
    else:
        angle = -angle_delta
    return angle


def radious(node_list):
    global_radius_dict = dict()
    for idx, node in enumerate(node_list):
        if (node_list[idx]["type"] == 'person'):
            r = r_Max
        else:
            r = r_Min
        radius_dict = {
            "r": r,
            "id": node_list[idx]["id"]
        }
        global_radius_dict[node_list[idx]["id"]] = radius_dict
    return global_radius_dict

# obliczanie odległości od wszystkich punktow
def count_E(node_source_list, node_target_list, edges, global_node_dict):

    sumE=0

    for idx1 in range(0, len(node_source_list)-1):
        pkt_node = [0,0,0]
        for idx2 in range(0, len(node_target_list)-1):

            node1_id = node_source_list[idx1]["id"]
            node2_id = node_target_list[idx2]["id"]

            #obliczenie odleglosci w ukl kartezianskim pomiedzy punktami
            pkt_node[0] = global_node_dict[node1_id]["x"] * global_node_dict[node2_id]["x"]
            pkt_node[1] = global_node_dict[node1_id]["y"] * global_node_dict[node2_id]["y"]
            pkt_node[2] = global_node_dict[node1_id]["z"] * global_node_dict[node2_id]["z"]
            distance = pkt_node[0]+pkt_node[1]+pkt_node[2]
            #print('distance', distance)

            #spr czy pomiedzy aktualnymi wezlami jest polaczenie
            wspolA = is_edge(node1_id, node2_id, edges)

            #obliczenie sumy wszystkich odleglosci
            sumE = sumE + math.pow((wspolA*r_Max*r_Min - distance), 2)

    #print('sumE', sumE)
    return sumE

# funkcja sprawdzajaca czy pomiedzy wezlami jest krawedz
def is_edge(id_source, id_target, edges):
    nodes = edges.filter(source_node__id=id_source, target_node__id=id_target).exists()
    if nodes == True:
        return 1
    else:
        return -1

# funkcja przemieszczająca punkt
def rotate_node(node_pkt, angle_delta, direction):
    angle = set_angle(direction, angle_delta)
    rotate_matrix = numpy.zeros((4, 4))
    if direction == 'N' or direction == 'S':
        rotate_matrix[0, 0] = numpy.cos(angle)
        rotate_matrix[2, 2] = numpy.cos(angle)
        rotate_matrix[0, 2] = numpy.sin(angle)
        rotate_matrix[2, 0] = -numpy.sin(angle)
        rotate_matrix[1, 1] = 1
        rotate_matrix[3, 3] = 1
    else:
        rotate_matrix[0, 0] = numpy.cos(angle)
        rotate_matrix[1, 1] = numpy.cos(angle)
        rotate_matrix[0, 1] = -numpy.sin(angle)
        rotate_matrix[1, 0] = numpy.sin(angle)
        rotate_matrix[2, 2] = 1
        rotate_matrix[3, 3] = 1

    #rotate_matrix = numpy.zeros((4, 4))
    point = numpy.ones((4,1))
    point[0, 0] = node_pkt[0]
    point[1, 0] = node_pkt[1]
    point[2, 0] = node_pkt[2]
    return numpy.dot(rotate_matrix,point)


