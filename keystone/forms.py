# -*- coding: utf-8 -*-
from django.forms import ChoiceField, Form
from .models import Node

__author__ = 'mateuszb'


class NodeFilterForm(Form):
    node_type = ChoiceField(choices=Node.TYPE_CHOICES, required=False,
                            label='Node type')


class EdgeFilterForm(Form):
    source_node_type = ChoiceField(choices=Node.TYPE_CHOICES,
                                   required=False,
                                   label='Source node type')
    target_node_type = ChoiceField(choices=Node.TYPE_CHOICES,
                                   required=False,
                                   label='Target node type')
