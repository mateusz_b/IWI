from collections import defaultdict

from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from django.views.generic import ListView, DetailView, TemplateView

from keystone.utils import calculate_points, count_E, direction_random, set_angle, radious, rotate_node
from .forms import EdgeFilterForm, NodeFilterForm
from .models import Edge, Node


class HomeView(TemplateView):
    template_name = 'home.html'


home_view = HomeView.as_view()


class VisualizationView(TemplateView):
    template_name = 'keystone/visualization.html'

    def get_context_data(self, **kwargs):
        context = super(VisualizationView, self).get_context_data(**kwargs)
        types = list(set(Node.objects.values_list('type', flat=True)))
        filter_dict = dict()
        for t in types:
            if t != 'person':
                filter_dict[t] = list(Node.objects.filter(type=t).order_by('name').values_list(
                    'name', flat=True))
        context.update({"targets": filter_dict.keys()})
        context.update({"form": NodeFilterForm()})
        context.update({"filters": filter_dict})
        return context


visualization_view = VisualizationView.as_view()


class EdgeListView(ListView):
    model = Edge
    paginate_by = 10

    def get_queryset(self):
        name = self.request.GET.get('name')
        source_type = self.request.GET.get('source_node_type')
        target_type = self.request.GET.get('target_node_type')
        if name:
            qs = Edge.objects.filter(Q(source_node__name__icontains=name) |
                                     Q(target_node__name__icontains=name))
        else:
            qs = Edge.objects.all()
        if source_type:
            qs = qs.filter(source_node__type=source_type)
        if target_type:
            qs = qs.filter(target_node__type=target_type)
        return qs

    def get_context_data(self, **kwargs):
        context = super(EdgeListView, self).get_context_data(**kwargs)
        context.update({"form": EdgeFilterForm()})
        return context


edge_list_view = EdgeListView.as_view()


class NodeListView(ListView):
    model = Node
    paginate_by = 10

    def get_queryset(self):
        name = self.request.GET.get('name')
        node_type = self.request.GET.get('node_type')
        if name:
            qs = Node.objects.filter(name__icontains=name)
        else:
            qs = Node.objects.all()
        if node_type:
            qs = qs.filter(type=node_type)
        return qs

    def get_context_data(self, **kwargs):
        context = super(NodeListView, self).get_context_data(**kwargs)
        context.update({"form": NodeFilterForm()})
        return context


node_list_view = NodeListView.as_view()


class NodeDetailView(DetailView):
    model = Node


node_detail_view = NodeDetailView.as_view()


class DataView(View):
    def get(self, request, *args, **kwargs):
        global_node_dict = dict()
        node_list = []
        edge_list = []
        distance_buf = []
        node_buf_dict = dict()

        #pomocnicze do obliczania E
        node_source_list = []
        node_target_list = []

        choice = request.GET.get('node_type', 'expertise')
        filters = request.GET.getlist('filters', None)

        #zmienna odpalająca algorytm
        if request.GET.get('algorytm'):
            filtersAlg = 1
        else:
            filtersAlg = 0

        nodes = Node.objects.filter(type='person')  # wsrod wszystkich wezlow, wybierz wszystkie te o type='person'
        # ogranicz węzły tylko do tych, które występują w relacjach person-expertise
        if not filters:
            nodes = [node for node in nodes if
                     Edge.objects.filter(source_node=node,
                                         target_node__type=choice).exists()]
        else:
            nodes = [node for node in nodes if
                     Edge.objects.filter(source_node=node,
                                         target_node__name__in=filters).exists()]

        # liczba punktów to liczba węzłów, które spełniają powyższe warunki
        n = len(nodes)
        # oblicz współrzędne punktów przy rozłożeniu w sposób równomierny,
        # radius_factor to zmienna skalująca sfery - sfera dla węzlow typu person powinna byc wieksza niz dla expertise
        points = calculate_points(n, radius_factor=2)
        # dla kazdego wezla stowrz slownik z odpowiednimi informacjami oraz jego pozycja i dodaj do ogolnej listy
        for idx, node in enumerate(nodes):
            pts = points[idx]
            node_dict = {
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "img_src": node.img_src,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            }
            global_node_dict[node.id] = node_dict
            node_list.append(node_dict)
            node_source_list.append(node_dict)

        # to samo dla innej ilosci wezlow
        nodes = Node.objects.filter(type=choice) if not filters else Node.objects.filter(name__in=filters)
        n = nodes.count()
        # tutaj juz nie skalujemy - mniejsza sfera
        points = calculate_points(n)
        for idx, node in enumerate(nodes):
            pts = points[idx]
            node_dict = {
                "id": node.id,
                "name": node.name,
                "type": node.type,
                "x": pts[0],
                "y": pts[1],
                "z": pts[2]
            }
            global_node_dict[node.id] = node_dict
            node_list.append(node_dict)
            node_target_list.append(node_dict)

        # znajdz wszystkie relacje person-expertise
        if not filters:
            edges = Edge.objects.filter(source_node__type='person',
                                        target_node__type=choice)
        else:
            edges = Edge.objects.filter(target_node__name__in=filters)


        # ALGORYTM -----------------------------------------------------------------------------------------------------
        if filtersAlg == 1:

            global minE
            minE = count_E(node_source_list, node_target_list, edges, global_node_dict)
            print(minE)

            angle = 30
            numIteration = int(angle/2)

            for i in range(0, numIteration):
                print("iteration ", i)
                # dla wszystkich wezlow
                for idx, node in enumerate(node_list):
                    # losujemy kierunek
                    direction = direction_random()
                    set_angle(direction, angle)

                    # zapamietujemy pkt
                    node_buf_dict[node_list[idx]["id"]] = global_node_dict[node_list[idx]["id"]]

                    # odczytulemyaktualne wspolrzedne pkt
                    pkt_node = [0, 0, 0]
                    node_id = node_list[idx]["id"]
                    pkt_node[0] = global_node_dict[node_id]["x"]
                    pkt_node[1] = global_node_dict[node_id]["y"]
                    pkt_node[2] = global_node_dict[node_id]["z"]

                    # przesuwamy pkt
                    pts = rotate_node(pkt_node, angle, direction)

                    copy_node = {
                         "id": node_id,
                         "name": global_node_dict[node_id]["name"],
                         "type": global_node_dict[node_id]["type"],
                         "x": global_node_dict[node_id]["x"],
                         "y": global_node_dict[node_id]["y"],
                         "z": global_node_dict[node_id]["z"],
                    }
                    global_node_dict[node_id]["x"] = pts[0][0]
                    global_node_dict[node_id]["y"] = pts[1][0]
                    global_node_dict[node_id]["z"] = pts[2][0]

                    newE = count_E(node_source_list, node_target_list, edges, global_node_dict)
                    #print("newE", newE)

                    # spr czy po przesunięcu minE się zmniejszyło, jeśli tak, to zapamiętujemy
                    # nowe połozenie, jeśli nie to przechodzimy dalej
                    if (newE > minE):
                        global_node_dict[node_id]["x"] = copy_node["x"]
                        global_node_dict[node_id]["y"] = copy_node["y"]
                        global_node_dict[node_id]["z"] = copy_node["z"]
                    else:
                         minE = newE

                # zminiejszenie kata w celu lepszej dokladnosci
                if i%5==0:
                    angle = angle/2

            print("minE", minE)
        # koniec algorytmu -------------------------------------------------------------------------

        #wyznaczanie wspolrzednych krawedzi
        for edge in edges:
            # w global_node_dict przechowujemy wspolrzedne wybrane dla danego wezla
            source_node = global_node_dict[edge.source_node.id]
            target_node = global_node_dict[edge.target_node.id]
            edge_list.append({
                "source": {
                    "x": source_node["x"],
                    "y": source_node["y"],
                    "z": source_node["z"],
                },
                "target": {
                    "x": target_node["x"],
                    "y": target_node["y"],
                    "z": target_node["z"],
                }
            })

        return JsonResponse([node_list, edge_list], safe=False)


data_view = DataView.as_view()


class NodeDataView(View):
    def get(self, request, *args, **kwargs):
        node = request.GET.get('node')
        node = Node.objects.get(name=node)
        related_nodes = node.source.all().values_list('target_node__type', 'target_node__name')
        related_nodes_dict = defaultdict(list)
        for node_type, name in related_nodes:
            related_nodes_dict[node_type].append(name)
        return JsonResponse(related_nodes_dict, safe=False)

node_data_view = NodeDataView.as_view()
