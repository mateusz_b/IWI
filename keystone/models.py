import uuid

from django.db import models


# Create your models here.
class Node(models.Model):
    TYPE_PERSON = 'person'
    TYPE_EXPERTISE = 'expertise'
    TYPE_WORKGROUP = 'workgroup'
    TYPE_ENTITY = 'entity'
    TYPE_CHOICES = (
        (TYPE_PERSON, 'Person'),
        (TYPE_EXPERTISE, 'Expertise'),
        (TYPE_WORKGROUP, 'Workgroup'),
        (TYPE_ENTITY, 'Entity')
    )

    id = models.CharField(max_length=200, primary_key=True)
    type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    href = models.CharField(null=True, blank=True, max_length=200)
    name = models.CharField(blank=True, null=True, max_length=200)
    img_src = models.CharField(blank=True, null=True, max_length=500)

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return "/node/%i/" % self.id

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.id = uuid.uuid5(uuid.NAMESPACE_DNS, self.name)
        super().save(force_insert, force_update, using, update_fields)


class Edge(models.Model):
    source_node = models.ForeignKey(Node, related_name='source')
    target_node = models.ForeignKey(Node, related_name='target')

    class Meta:
        unique_together = ('source_node', 'target_node')
