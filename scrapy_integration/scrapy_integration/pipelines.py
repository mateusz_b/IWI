# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import uuid

from django.db import IntegrityError
from scrapy_integration.items import AuthorNodeItem, FilterNodeItem
from keystone.models import Node, Edge


class ScrapyIntegrationPipeline(object):
    def process_item(self, item, spider):
        if isinstance(item, AuthorNodeItem):
            country = item.pop('country')
            obj = item.save()
            country_node, _ = Node.objects.get_or_create(name=country,
                                                         type='country')
            try:
                Edge.objects.create(source_node=obj, target_node=country_node)
            except IntegrityError:
                pass
        elif isinstance(item, FilterNodeItem):
            node_id = uuid.uuid5(uuid.NAMESPACE_DNS, item['name'])
            try:
                node = Node.objects.get(id=node_id, type=item['type'])
            except Node.DoesNotExist:
                node = Node.objects.create(name=item['name'],
                                           type=item['type'],
                                           href=item['href'])
            for person_id in item['people']:
                try:
                    person = Node.objects.get(id=person_id)
                except Node.DoesNotExist:
                    print("Node with id %s does not exist" % person_id)
                    continue
                try:
                    Edge.objects.create(source_node=person, target_node=node)
                except IntegrityError:
                    pass
        return item
