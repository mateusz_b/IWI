# -*- coding: utf-8 -*-

import scrapy
import uuid

from scrapy_integration.items import AuthorNodeItem

__author__ = 'mateuszb'


class AuthorsSpider(scrapy.Spider):
    name = 'authors'
    start_urls = ["http://www.keystone-cost.eu/keystone/members/"]

    def parse(self, response):
        for author in response.css('div#memberslist ul li'):
            yield AuthorNodeItem(**{
                "img_src": author.css(
                    'div.photo-container a img::attr(src)').extract_first(),
                "href": author.css(
                    'div.photo-container a::attr(href)').extract_first(),
                "name": author.xpath('div[last()]/a/text()').extract_first(),
                # "id": str(uuid.uuid5(uuid.NAMESPACE_DNS, author.xpath(
                #     'div[last()]/a/text()').extract_first())),
                "country": author.xpath(
                    'div[last()]/text()').extract_first() or "Not specified",
                "type": "person"
            })
