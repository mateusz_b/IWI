# -*- coding: utf-8 -*-
import uuid

import scrapy
from scrapy_integration.items import FilterNodeItem

__author__ = 'mateuszb'


class FiltersSpider(scrapy.Spider):
    name = 'filters'
    start_urls = ["http://www.keystone-cost.eu/keystone/members/"]

    def parse(self, response):
        workgroups = response.xpath("//div[@class='cat-list-filter'][1]")
        expertises = response.xpath("//div[@class='cat-list-filter'][2]")
        expectations = response.xpath("//div[@class='cat-list-filter'][3]")
        for workgroup in workgroups.css('li.cat-item a'):
            response_dict = {
                "name": workgroup.css('::text').extract_first(),
                "href": workgroup.css('::attr(href)').extract_first(),
                "type": "workgroup"
            }
            request = scrapy.Request(response_dict["href"],
                                     callback=self.parse_people_on_page)
            request.meta['response_dict'] = response_dict
            yield request

        for expertise in expertises.css('li.cat-item a'):
            response_dict = {
                "name": expertise.css('::text').extract_first(),
                "href": expertise.css('::attr(href)').extract_first(),
                "type": "expertise"
            }
            request = scrapy.Request(response_dict["href"],
                                     callback=self.parse_people_on_page)
            request.meta['response_dict'] = response_dict
            yield request

        for expectation in expectations.css('li.cat-item a'):
            response_dict = {
                "name": expectation.css('::text').extract_first(),
                "href": expectation.css('::attr(href)').extract_first(),
                "type": "expectation"
            }
            request = scrapy.Request(response_dict["href"],
                                     callback=self.parse_people_on_page)
            request.meta['response_dict'] = response_dict
            yield request

    def parse_people_on_page(self, response):
        response_dict = response.meta["response_dict"]
        response_dict["people"] = []
        for author in response.css('div#memberslist ul li'):
            try:
                response_dict["people"].append(
                    str(uuid.uuid5(uuid.NAMESPACE_DNS, author.xpath(
                        'div/a/text()').extract_first())),
                    )
            except TypeError:
                continue
        return FilterNodeItem(**response_dict)
